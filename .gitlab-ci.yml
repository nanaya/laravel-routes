cache:
  key: "%CI_PROJECT_ID%"
  paths:
    - composer_cache # cache the composer directory

variables:
  COMPOSER_CACHE_DIR: composer-cache

# set the default docker image
image: php:7.2

stages:
  - check
  - test
  - deploy

# Templates

.before_nocov: &before_nocov
  before_script:
    # Install git, the php image doesn't have installed
    - apt-get update -yqq
    - apt-get install git zip zlib1g-dev -yqq
    # Install composer
    - curl -sS https://getcomposer.org/installer | php

.before_pcov: &before_pcov
  before_script:
    # Install git, the php image doesn't have installed
    - apt-get update -yqq
    - apt-get install git zip zlib1g-dev -yqq
    # Install composer
    - curl -sS https://getcomposer.org/installer | php
    # Install pcov
    - pecl install pcov
    # Enable pcov
    - docker-php-ext-enable pcov


# Check stage
lint:
  stage: check
  script:
    # lint recursive
    - find src/ -type f -name '*.php' -exec php -l {} \; | (! grep -v "No syntax errors detected" )
  tags:
    - docker

# We run phpmd in a seperated job
phpmd:
  image: phpqa/phpmd
  stage: check
  script:
    - phpmd src/ text codesize,naming,design,unusedcode

phpcs:
  image: phpqa/phpcs
  stage: check
  script:
    # check for codestyle errors
    - phpcs --standard=PSR2 src/
  tags:
    - docker

eslintJavascript:
  stage: check
  services:
    - node
  before_script:
    - curl -sL https://deb.nodesource.com/setup_10.x | bash -
    - apt install nodejs -y
    - npm install -g yarn
    - yarn
  script:
    - yarn codestyle
  artifacts:
    name: "codestyle"
    when: always
    paths:
      - codestyle.html
    expire_in: 2 hour

# Test stage

# we have some statistics jobs in the test stage because we don't need to wait on it before we start the tests
phploc:
  image: phpqa/phploc
  stage: test
  script:
    - mkdir -p phploc
    - phploc src/ --log-xml=phploc/phploc.xml --log-csv=phploc/phploc.csv
  artifacts:
    name: "phploc"
    paths:
      - phploc/*
    expire_in: 2 hour
  tags:
    - docker

phpmetrics:
  image: phpqa/phpmetrics
  stage: test
  script:
    - phpmetrics --report-html=phpmetrics ./src
  artifacts:
    name: "phpmetrics"
    when: always
    paths:
      - phpmetrics/*
    expire_in: 2 hour
  tags:
    - docker

# We test PHP7.2
test:7.2:
  <<: *before_pcov
  stage: test
  script:
    - mkdir phpunitresult
    - php composer.phar install
    - vendor/bin/phpunit --coverage-text --colors=never --coverage-html=phpunitresult/coverage --testdox-html=phpunitresult/testdox.html --testdox-text=phpunitresult/testdox.txt  --log-junit=phpunitresult/junit.xml
  artifacts:
    name: "phpunitresult"
    when: always
    paths:
      - phpunitresult/*
    expire_in: 2 hour
    reports:
      junit: phpunitresult/junit.xml

testJavaScript:
  stage: test
  services:
    - node
  before_script:
    - curl -sL https://deb.nodesource.com/setup_10.x | bash -
    - apt install nodejs -y
    - npm install -g yarn
    - yarn
  script:
    - yarn test
  artifacts:
    name: "codecoverage"
    when: always
    paths:
      - coverage/*
    expire_in: 2 hour

# We test PHP7.3
test:7.3:
  <<: *before_pcov
  stage: test
  image: php:7.3
  script:
    - php composer.phar install
    - vendor/bin/phpunit --coverage-text --colors=never --log-junit=junit_73.xml
  artifacts:
    expire_in: 1 hour
    reports:
      junit: junit_73.xml

test:7.2_lowest:
  <<: *before_pcov
  stage: test
  script:
    - php composer.phar update --prefer-lowest
    - vendor/bin/phpunit --coverage-text --colors=never --log-junit=junit_72_lowest.xml
  artifacts:
    expire_in: 1 hour
    reports:
      junit: junit_72_lowest.xml

test:7.3_laravel_5.8:
  <<: *before_pcov
  stage: test
  image: php:7.3
  script:
    - php composer.phar require --dev "orchestra/testbench=~3.8"
    - vendor/bin/phpunit --coverage-text --colors=never --log-junit=junit_73_laravel_58.xml
  artifacts:
    expire_in: 1 hour
    reports:
      junit: junit_73_laravel_58.xml

test:7.2_laravel_6.0:
  <<: *before_pcov
  stage: test
  script:
    - php composer.phar require --dev "orchestra/testbench=~3.9"
    - vendor/bin/phpunit --coverage-text --colors=never --log-junit=junit_72_laravel_60.xml
  artifacts:
    expire_in: 1 hour
    reports:
      junit: junit_72_laravel_60.xml

test:7.3_laravel_6.0:
  <<: *before_pcov
  stage: test
  image: php:7.3
  script:
    - php composer.phar require --dev "orchestra/testbench=~3.9"
    - vendor/bin/phpunit --coverage-text --colors=never --log-junit=junit_73_laravel_60.xml
  artifacts:
    expire_in: 1 hour
    reports:
      junit: junit_73_laravel_60.xml

# Deploy stage
combinebuilddata:
  stage: deploy
  when: always
  tags:
    - docker
  script:
    - mkdir -p build
    - touch build/placeholder
    - mkdir -p phpunitresult
    - touch phpunitresult/placeholder
    - mkdir -p phploc
    - touch phploc/placeholder
    - mkdir -p phpmetrics
    - touch phpmetrics/placeholder
    - mv phpunitresult/* build
    - mv phploc/* build
    - mv phpmetrics build/phpmetrics
    - mkdir -p build/coverage
    - mv coverage/* build/coverage/
    - mkdir -p build/codestyle
    - mv codestyle.html build/codestyle/index.html
  artifacts:
    name: "Build ${CI_PROJECT_NAME}_${CI_PIPELINE_ID}"
    when: always
    paths:
      - build/*
    expire_in: 1 day
