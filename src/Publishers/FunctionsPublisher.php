<?php

namespace BlueDragon\LaravelRoutes\Publishers;

use BlueDragon\LaravelRoutes\Creators\FunctionCreatorInterface;
use BlueDragon\LaravelRoutes\Exceptions\LaravelRoutesException;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Filesystem\Filesystem;

/**
 * Class FunctionsPublisher
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 */
class FunctionsPublisher implements FunctionsPublisherInterface
{
    /**
     * @var \Illuminate\Config\Repository
     */
    protected $config;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var FunctionCreatorInterface
     */
    protected $functionCreator;

    /**
     * FunctionsPublisher constructor.
     *
     * @param Repository $config
     * @param FunctionCreatorInterface $functionCreator
     * @param Filesystem $filesystem
     */
    public function __construct(
        Repository $config,
        FunctionCreatorInterface $functionCreator,
        Filesystem $filesystem
    ) {
        $this->config = $config;
        $this->filesystem = $filesystem;
        $this->functionCreator = $functionCreator;
    }

    /**
     * Publish the functions file
     *
     * @param string|null $filename
     * @param string|null $path
     *
     * @throws LaravelRoutesException
     *
     * @return void
     */
    public function publish(string $filename = null, string $path = null) : void
    {
        if ($filename === null) {
            $filename = $this->config->get('laravel-routes.script_functions_filename');
        }
        if ($path === null) {
            $path = $this->config->get('laravel-routes.export_directory');
        }

        try {
            $this->filesystem->makeDirectory($path, 0755, true, true);
            $this->filesystem
                ->put(
                    $path . DIRECTORY_SEPARATOR . $filename,
                    $this->functionCreator->getScript()
                );
        } catch (\Throwable $error) {
            throw new LaravelRoutesException('Something went wrong', 0, $error);
        }
    }
}
