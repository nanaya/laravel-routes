<?php

namespace BlueDragon\LaravelRoutes\Creators;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Routing\Route;
use Illuminate\Routing\Router;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

/**
 * Class RoutesDataCreator
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 */
class RoutesDataCreator implements RoutesDataCreatorInterface
{
    /**
     * @var Repository
     */
    protected $config;

    /**
     * @var Router
     */
    protected $router;

    /**
     * RoutesDataCreator constructor.
     *
     * @param Repository $config
     * @param Router $router
     */
    public function __construct(Repository $config, Router $router)
    {
        $this->config = $config;
        $this->router = $router;
    }

    /**
     * @param string|null $group
     *
     * @return Collection
     */
    public function getRoutesData(string $group) : Collection
    {
        $routes = new Collection($this->router->getRoutes());

        // we will use the route
        $groupConfig = $this->config->get('laravel-routes.route_groups.' . $group, []);

        return $routes
            ->filter(function ($route) use ($group, $groupConfig) {
                return $this->filterRoute($route, $group, $groupConfig);
            })
            ->mapWithKeys(function ($route) {
                return $this->mapRouteData($route);
            });
    }

    /**
     * @param Route $route
     * @param string $group
     * @param array $groupConfig
     *
     * @return bool
     */
    protected function filterRoute(Route $route, string $group, array $groupConfig) : bool
    {
        $action = $route->getAction();
        if (!$action || !isset($action['as'])) {
            // we don't add routes without a name
            return false;
        }
        $keyname = $this->config->get('laravel-routes.route_keyname');
        if (isset($action[$keyname]) && $action[$keyname] === $group) {
            // this route need to be added on the current group
            return true;
        }
        if (isset($action[$keyname]) && $action[$keyname] === true && $groupConfig['include_true']) {
            // this group include all the routes with true
            return true;
        }
        // we check if the route is added
        return Arr::get($groupConfig, 'routes.'.$action['as'], false);
    }

    /**
     * @param Route $route
     *
     * @return array
     */
    protected function mapRouteData(Route $route): array
    {
        return [
            $route->getName() => [
                'methods' => $route->methods(),
                'uri' => $route->uri(),
            ],
        ];
    }
}
