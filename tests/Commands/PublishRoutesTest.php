<?php


namespace BlueDragon\LaravelRoutes\Tests\Commands;


use BlueDragon\LaravelRoutes\Exceptions\LaravelRoutesException;
use BlueDragon\LaravelRoutes\Publishers\RoutesPublisherInterface;
use BlueDragon\LaravelRoutes\Tests\TestCase;

/**
 * Class PublishRoutesTest
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 *
 * @group commands
 * @group routes
 */
class PublishRoutesTest extends TestCase
{
    /**
     *
     * @return void
     *
     * @test
     */
    public function we_can_call_the_command_with_default_options(): void
    {
        $this->mock(RoutesPublisherInterface::class, function ($mock) {
            $mock->shouldReceive('publish')
                ->with(null, null, null)
                ->once()
            ;
        });
        $this->artisan('laravel-routes:publishroutes')
            ->expectsOutput('Routes published')
            ->assertExitCode(0)
        ;
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_get_a_nice_error_if_something_failed(): void
    {
        $this->mock(RoutesPublisherInterface::class, function ($mock) {
            $mock->shouldReceive('publish')
                ->with(null, null, null)
                ->once()
                ->andThrow(new LaravelRoutesException('test exception'))
                ;
        });
        $this->artisan('laravel-routes:publishroutes')
            ->expectsOutput('Sorry we have an exception: test exception')
            ->assertExitCode(1)
        ;
    }
}