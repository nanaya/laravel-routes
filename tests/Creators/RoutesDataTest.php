<?php


namespace BlueDragon\LaravelRoutes\Tests\Creators;

use BlueDragon\LaravelRoutes\Creators\RoutesDataCreator;
use BlueDragon\LaravelRoutes\Tests\TestCase;
use Illuminate\Config\Repository;
use Illuminate\Routing\Router;

/**
 * Class RoutesDataTest
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 *
 * @group creators
 * @group routes
 */
class RoutesDataTest extends TestCase
{

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_get_an_empty_set_if_there_isnt_any_route(): void
    {
        $router = $this->app->make(Router::class);
        $config = config();
        $creator = new RoutesDataCreator($config, $router);

        $this->assertEmpty($creator->getRoutesData('default'));
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_get_a_route_that_is_in_the_current_group(): void
    {
        /**
         * @var Router $router
         */
        $router = $this->app->make(Router::class);
        $config = $this->mock(Repository::class, function ($mock){
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_groups.groupname', [])
                ->once()
                ->andReturn([
                    'include_true' => false,
                    'routes' => [],
            ]);
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_keyname')
                ->andReturn('laroute_publish');
        });

        $router->get('test', ['uses' => function () {
                return 'result';
            }, 'laroute_publish' => 'groupname'])
            ->name('testroute');

        $creator = new RoutesDataCreator($config, $router);
        $result = $creator->getRoutesData('groupname');
        $this->assertTrue($result->has('testroute'));
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_dont_get_a_route_that_is_not_in_the_current_group(): void
    {
        /**
         * @var Router $router
         */
        $router = $this->app->make(Router::class);
        $config = $this->mock(Repository::class, function ($mock){
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_groups.groupname', [])
                ->once()
                ->andReturn([
                    'include_true' => false,
                    'routes' => [],
                ]);
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_keyname')
                ->andReturn('laroute_publish');
        });

        $router->get('test', ['uses' => function () {
            return 'result';
        }, 'laroute_publish' => 'othergroup'])
            ->name('testroute');

        $creator = new RoutesDataCreator($config, $router);
        $result = $creator->getRoutesData('groupname');
        $this->assertEmpty($result);
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_dont_get_a_route_without_a_name(): void
    {
        /**
         * @var Router $router
         */
        $router = $this->app->make(Router::class);
        $config = $this->mock(Repository::class, function ($mock){
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_groups.groupname', [])
                ->once()
                ->andReturn([
                    'include_true' => false,
                    'routes' => [],
                ]);
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_keyname')
                ->andReturn('laroute_publish');
        });

        $router->get('test', ['uses' => function () {
            return 'result';
        }, 'laroute_publish' => 'groupname']);

        $creator = new RoutesDataCreator($config, $router);
        $result = $creator->getRoutesData('groupname');
        $this->assertEmpty($result);
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_get_routes_with_true_if_including_true(): void
    {
        /**
         * @var Router $router
         */
        $router = $this->app->make(Router::class);
        $config = $this->mock(Repository::class, function ($mock){
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_groups.groupname', [])
                ->once()
                ->andReturn([
                    'include_true' => true,
                    'routes' => [],
                ]);
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_keyname')
                ->andReturn('laroute_publish');
        });

        $router->get('test', ['uses' => function () {
            return 'result';
        }, 'laroute_publish' => true])
            ->name('testroute');

        $creator = new RoutesDataCreator($config, $router);
        $result = $creator->getRoutesData('groupname');
        $this->assertTrue($result->has('testroute'));
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_dont_get_routes_with_true_if_including_false(): void
    {
        /**
         * @var Router $router
         */
        $router = $this->app->make(Router::class);
        $config = $this->mock(Repository::class, function ($mock){
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_groups.groupname', [])
                ->once()
                ->andReturn([
                    'include_true' => false,
                    'routes' => [],
                ]);
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_keyname')
                ->andReturn('laroute_publish');
        });

        $router->get('test', ['uses' => function () {
            return 'result';
        }, 'laroute_publish' => true])
            ->name('testroute');

        $creator = new RoutesDataCreator($config, $router);
        $result = $creator->getRoutesData('groupname');
        $this->assertEmpty($result);
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_can_change_the_key_to_search_for_in_the_routes()
    {
        $keyName = 'routeskeyname' . mt_rand();
        /**
         * @var Router $router
         */
        $router = $this->app->make(Router::class);
        $config = $this->mock(Repository::class, function ($mock) use ($keyName) {
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_groups.groupname', [])
                ->once()
                ->andReturn([
                    'include_true' => false,
                    'routes' => [],
                ]);
            $mock->shouldReceive('get')
                ->with('laravel-routes.route_keyname')
                ->andReturn($keyName);
        });

        $router->get('test', ['uses' => function () {
            return 'result';
        }, $keyName => 'groupname'])
            ->name('testroute');

        $creator = new RoutesDataCreator($config, $router);
        $result = $creator->getRoutesData('groupname');
        $this->assertTrue($result->has('testroute'));
    }
}
