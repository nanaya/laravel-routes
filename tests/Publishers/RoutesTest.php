<?php


namespace BlueDragon\LaravelRoutes\Tests\Publishers;


use BlueDragon\LaravelRoutes\Creators\RoutesCreatorInterface;
use BlueDragon\LaravelRoutes\Exceptions\LaravelRoutesException;
use BlueDragon\LaravelRoutes\Publishers\RoutesPublisher;
use BlueDragon\LaravelRoutes\Tests\TestCase;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Filesystem\Filesystem;

/**
 * Class RoutesTest, test that we can publish the routes files
 *
 * @author Tobias van Beek <t.vanbeek@bluedragon.nl>
 *
 * @group publishers
 * @group routes
 */
class RoutesTest extends TestCase
{

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_can_publish_the_routes(): void
    {
        $config = $this->mock(Repository::class, function ($mock){
            $mock->shouldReceive('get')
                ->with('laravel-routes.default_group')
                ->atLeast()
                ->once()
                ->andReturn('default')
            ;
            $mock->shouldReceive('get')
                ->with('laravel-routes.routes_file_basename')
                ->once()
                ->andReturn('routes')
            ;
            $mock->shouldReceive('get')
                ->with('laravel-routes.routes_file_extension')
                ->once()
                ->andReturn('json')
            ;
            $mock->shouldReceive('get')
                ->with('laravel-routes.export_directory')
                ->once()
                ->andReturn('path')
            ;
        });
        $creator = $this->mock(RoutesCreatorInterface::class, function ($mock) {
            $mock->shouldReceive('getRoutesScript')
                ->once()
                ->andReturn('testscript')
            ;
        });

        $filesystem = $this->mock(Filesystem::class, function ($mock) {
            $mock->shouldReceive('makeDirectory');
            $mock->shouldReceive('put')
                ->with('path' . DIRECTORY_SEPARATOR . 'routes.json', 'testscript')
                ->once()
            ;
        });

        $publisher = new RoutesPublisher($config, $filesystem, $creator);
        $publisher->publish();
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_get_the_group_name_in_the_file_if_it_isnt_the_default_group(): void
    {
        $config = $this->mock(Repository::class, function ($mock){
            $mock->shouldReceive('get')
                ->with('laravel-routes.default_group')
                ->atLeast()
                ->once()
                ->andReturn('default')
            ;
            $mock->shouldReceive('get')
                ->with('laravel-routes.routes_file_basename')
                ->once()
                ->andReturn('routes')
            ;
            $mock->shouldReceive('get')
                ->with('laravel-routes.routes_file_extension')
                ->once()
                ->andReturn('json')
            ;
            $mock->shouldReceive('get')
                ->with('laravel-routes.export_directory')
                ->once()
                ->andReturn('path')
            ;
        });
        $creator = $this->mock(RoutesCreatorInterface::class, function ($mock) {
            $mock->shouldReceive('getRoutesScript')
                ->once()
                ->andReturn('testscript')
            ;
        });

        $filesystem = $this->mock(Filesystem::class, function ($mock) {
            $mock->shouldReceive('makeDirectory');
            $mock->shouldReceive('put')
                ->with('path' . DIRECTORY_SEPARATOR . 'routes_other.json', 'testscript')
                ->once()
            ;
        });

        $publisher = new RoutesPublisher($config, $filesystem, $creator);
        $publisher->publish('other');
    }

    /**
     *
     * @return void
     *
     * @test
     */
    public function we_convert_any_error_to_a_package_exception(): void
    {
        $this->expectException(LaravelRoutesException::class);

        $config = $this->mock(Repository::class, function ($mock){
            $mock->shouldReceive('get')
                ->with('laravel-routes.default_group')
                ->atLeast()
                ->once()
                ->andReturn('default')
            ;
            $mock->shouldReceive('get')
                ->with('laravel-routes.routes_file_basename')
                ->once()
                ->andReturn('routes')
            ;
            $mock->shouldReceive('get')
                ->with('laravel-routes.routes_file_extension')
                ->once()
                ->andReturn('json')
            ;
            $mock->shouldReceive('get')
                ->with('laravel-routes.export_directory')
                ->once()
                ->andReturn('path')
            ;
        });
        $creator = $this->mock(RoutesCreatorInterface::class);

        $filesystem = $this->mock(Filesystem::class, function ($mock) {
            $mock->shouldReceive('makeDirectory')
                ->once()
                ->andThrow(new \Exception('test'))
            ;
        });

        $publisher = new RoutesPublisher($config, $filesystem, $creator);
        $publisher->publish();
    }
}